import  axios from "axios";

const API_URL = "http://localhost:8080/api/public"

class ServiceSearchInfoBricoleur {

    findUserInfo(id){
        return axios.get(API_URL +"/bricoleur/search-id/"+ id);
    }

    findSkills(id){
        return axios.get(API_URL +"/souscategories/bricoleur/"+ id);
    }

    // findUserInfoJWT(token){
    //     return axios.post("http://localhost:8080/api/user/info",token)
    // }

}
export default new ServiceSearchInfoBricoleur();