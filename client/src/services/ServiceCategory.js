import  axios from "axios";

const API_URL = "http://localhost:8080/api/public/"

class ServiceCategory {

    showAllCategories(){
        return axios.get(API_URL +"categories");
    }

    showAllSousCategories(){
        return axios.get(API_URL + "sousCategories")
    }

}
export default new ServiceCategory();