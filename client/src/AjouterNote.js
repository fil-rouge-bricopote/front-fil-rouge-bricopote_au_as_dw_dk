import axios from 'axios';
import React, { useState } from 'react';
//import Button from 'react-bootstrap/Button'
//import Modal from 'react-bootstrap/Modal'
function AjouterNote(props) {

    const [isOpen, setIsOpen] = useState(false);

    const closeModal = () => {
        setIsOpen(false)
    }

    const openModal = () => {
        setIsOpen(true)
    }

    

    const [note, setNote] = useState({ note: '', avis: '', idBricoleur : 0 });
    const [message, setMessage] = useState("");
    const envoyerNote = e => {
        e.preventDefault();
        
        console.log(note.note + ' ' + note.avis + ' ' + note.idBricoleur);
        axios.post('http://localhost:8080/notes/addNote', note, {
            headers:
                { 'Content-type': 'application/json;charset=utf-8' }
        }).then(res => { console.log(res.data) }).catch(error => { console.log(error) })
        setMessage("succés");
        setNote({ note: '', avis: '', idBricoleur : 0 })
        closeModal()
    }
    const recuperDonnees = e => {
        setNote({
            ...note,
            [e.target.name]: e.target.value,
            idBricoleur : props.idBricopote
        });

        

        e.preventDefault();
    }
    return (
        <>
        {console.log(props)}
            <div className="flex items-center justify-center">
                <button
                    type="button"
                    onClick={openModal}
                    className="px-4 py-2 w-full mt-2 text-sm font-bold uppercase text-white bg-yellowBricopote rounded-md hover:bg-opacity-70 focus:outline-none"
                >
                    Donner son Avis
                </button>
            </div>

            <div className={`modal ${isOpen ? 'show' : ''} `}>
                <div className="modal-content rounded-xl shadow-2xl flex flex-col justify-center">
                    <div className="modal-header text-center">
                        <h4 className="modal-title text-xl font-bold ">Ajouter un avis</h4>
                    </div>
                    <div className="modal-body flex justify-center">
                        <form onSubmit={envoyerNote}>
                            <div className="mb-2 flex flex-col justify-center">
                                <label htmlFor="inputNote" className="text-lg">Veuillez selection une note allant de 1 à 5 </label>

                                <input type="number" min="1" max="5" id="inputNote" className="border-2 border-yellowBricopote " onChange={recuperDonnees} value={note.note} name="note" required  defaultValue={1}/>
                            </div>
                            <div className="flex flex-col justify-center mb-2">
                                <label htmlFor="inputAvis" className="text-lg"> Redigez votre avis </label>
                                <br/>
                                <textarea id="inputAvis" rows="4" className="border-2 border-yellowBricopote" onChange={recuperDonnees} value={note.avis} name="avis"></textarea>
                            </div>
                            <div className = "flex justify-center p-2">
                                <input type="submit" value="Enregistrer" className="bg-blue-500 rounded-lg p-2 text-white font-bold" />
                                <button className="bg-green-600 rounded-lg mx-4 p-2 text-white font-bold" onClick={closeModal}>           Fermer
                                </button>
                            </div>

                        </form>

                    </div>
                    <div className="modal-footer">


                    </div>
                </div>
            </div>



        </>
    );
}
export default AjouterNote;