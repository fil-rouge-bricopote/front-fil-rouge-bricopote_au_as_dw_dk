import React,{useEffect,useState} from 'react'
import CatDropDowunList from './CatDropDowunList'

const UserSearchBar = ({setParams}) => {
    const [cat,setCat] = useState('')
    const [keyWord,setKeyWord] = useState('')
    const [ville,setVille] = useState('')

    const [cat1,setLocalCat] = useState('')
    const [key1,setLocalKeyWord] = useState('')
    const [ville1,setLocalVille] = useState('')

    const style = "rounded-full w-full py-4 px-6 text-gray-700 leading-tight focus:outline-none"
    
    const handleChange = ({target}) => {
        switch (target.name) {
            case 'key':
                setKeyWord(target.value)
                break;
            case 'ville':
                setVille(target.value)
                break;
            default:
                break;
        }   
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        setParams(`cat=${cat}&ville=${ville}&key=${keyWord}`)
    }
    
    return (
        <form onSubmit={handleSubmit} className="bg-white flex items-center rounded-full shadow-2xl">
            <input onChange={handleChange} className={`${style} `} type="text" name="key" placeholder="nom , prenom , tel ou email"/>
            <CatDropDowunList style={style} setCat={setCat}/>
            <input onChange={handleChange} className={`${style} `} type="text" name="ville" placeholder="Ville ou CP"/>
            <input className={`${style} hover:bg-gray-400 hover:shadow-inner`} type="submit" value="Rechercher" />
        </form>
    )
}

export default UserSearchBar

