import React from 'react'

function ImageCard({img}) {
    
    const pathImg ="http://127.0.0.1:3000/pi/n/";
    const pathImgSm = "http://127.0.0.1:3000/pi/r/";
    const defaultImg ="http://127.0.0.1:3000/img/defaultImg.jpg";

    const url = img !=null && img !="" ? pathImg+img : defaultImg;

    return <img alt="avatar" className="w-full h-56 object-cover object-center" src={url}/>;
}

export default ImageCard
