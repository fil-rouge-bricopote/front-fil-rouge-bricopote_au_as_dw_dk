import React, { Component } from 'react';
import ImageCard from './ImageCard';
import DetailProfilCard from './DetailProfilCard';
import { Link } from 'react-router-dom';


export class ProfileCard extends Component {
    render() {
        const bricopot = this.props.bricopot;
        return (
            <Link to={{ pathname: `/profil/${bricopot.idUser}` }}>
                <div className="
            w-72
            bg-white
            shadow-lg
            rounded-md
            overflow-hidden
            my-4
            mx-auto
          ">
                    <ImageCard img={bricopot.imageProfil} />
                    <DetailProfilCard bricopot={bricopot} />
                </div>
            </Link>
        )
    }
}

export default ProfileCard
