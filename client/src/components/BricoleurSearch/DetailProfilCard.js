import React from 'react';
import location_icon from '../../img/profilCard/location.svg';
import call_icon from '../../img/profilCard/call.svg';
import email_icon from '../../img/profilCard/email.svg';

function DetailProfilCard({bricopot}) {
    return (
        <div className="py-4 px-6">
                <h1 className="text-2xl text-center font-semibold text-gray-800">
                    {`${bricopot.nom.toUpperCase()} ${bricopot.prenom}`}
                </h1>

                <div className="mx-auto w-4/5 pt-3 border-b-2 border-gray-800 opacity-25"></div>

                <div className="flex items-center mt-4 text-gray-700">
                    <img className="h-6 w-6" src={location_icon} alt="location_icon" srcset="" />
                    <h1 className="px-2 text-sm"> {`${bricopot.adresse.cp} ${bricopot.adresse.ville}`}</h1>
                </div>
                <div className="flex items-center mt-4 text-gray-700">
                    <img className="h-6 w-6" src={call_icon} alt="call_icon" srcset="" />
                    <h1 className="px-2 text-sm">{bricopot.tel}</h1>
                </div>
                <div className="flex items-center mt-4 text-gray-700">
                    <img className="h-6 w-6" src={email_icon} alt="email_icon" srcset="" />
                    <h1 className="px-2 text-sm">{bricopot.email}</h1>
                </div>
            </div>
    )
}

export default DetailProfilCard
