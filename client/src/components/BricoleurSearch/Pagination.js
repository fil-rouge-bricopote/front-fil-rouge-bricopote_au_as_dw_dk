import React from "react";

const Pagination = ({ page, setPage, nbPages }) => {
  const style_li =
    "relative block leading-tight text-black bg-white rounded-full shadow-xl border";
  const style_btn = "rounded-full w-full py-4 px-6 text-gray-700 leading-tight focus:outline-none" 
  // "py-2 px-3 focus:outline-none";
  const style_btn_disable = "bg-gray-300 hover:none";
  const style_btn_enable = "bg-white hover:bg-gray-200";
  const style = "rounded-full w-full py-4 px-6 text-gray-700 leading-tight focus:outline-none"
  const getBtnStyle = (i) =>
    `${style_btn} ${i === page ? style_btn_disable : style_btn_enable}`;

  const builder = () => {
    const pages = [];
    for (let i = 0; i < nbPages; i++) {
      pages.push(
        <li key={i} className={style_li}>
          <button
            disabled={page === i ? true : false}
            onClick={handlePageChange}
            className={getBtnStyle(i)}
            id={i}
          >
            {i + 1}
          </button>
        </li>
      );
    }
    return pages;
  };

  const handlePageChange = (e) => {
    const id = parseInt(e.target.id);
    if (page < nbPages && page >= 0) {
      if (page !== id) {
        setPage(id);
      }
    }
  };

  const getNext = () => (page + 1 < nbPages ? page + 1 : nbPages - 1);
  const getPrevious = () => (page - 1 > 0 ? page - 1 : 0);

  return (
    <div className="w-min m-auto">
      <ul className="flex pl-0 list-none rounded my-2">
        <li className={`${style_li} `}>
          <button
            disabled={page <= 0 ? true : false}
            onClick={handlePageChange}
            id={getPrevious()}
            className={getBtnStyle(getPrevious())}
          >
            Previous
          </button>
        </li>

        {builder()}

        <li className={`${style_li} `}>
          <button
            disabled={page + 1 <= nbPages ? false : true}
            onClick={handlePageChange}
            id={getNext()}
            className={getBtnStyle(getNext())}
          >
            Next
          </button>
        </li>
      </ul>
    </div>
  );
};

export default Pagination;
