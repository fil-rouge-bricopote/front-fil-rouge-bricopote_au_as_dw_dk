import { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { Disclosure } from '@headlessui/react'
import { MenuIcon, XIcon } from '@heroicons/react/outline'
import LogoBricoPote from './LogoBricoPote'
import UserProfil from './UserProfil'
import './NavBar.css'
import axios from 'axios'




const navigationOffline = [
  { name: 'Devenir bricopote', route: '/inscription/0' }
]

const navigationConnexion = [
  { name: 'Inscription', route: '/inscription/1' },
  { name: 'Connexion', route: '/login' }
]

const navigationBricoleur = [
  { name: 'Poster une annonce', route: '/posterAnnonce' },
  { name: 'Rechercher des annonces', route: '/annonces' }
]

const navigationDashboard = [
  { name: 'Mes Infos', route: '/dashboard/monProfil' },
  { name: 'Ma messagerie', route: '/dashboard/messagerie' }
]

const navigationUtilisateur = [
  { name: 'Poster une annonce', route: '/posterAnnonce' },
  { name: 'Devenir bricopote', route: '/inscription/0' }
]

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}


const Navbar = (props) => {

  let history = useHistory();
  let url = 'http://localhost:8080/api/user/info/role'

  const [role, setRole] = useState("");
  const [navigation, setNavigation] = useState(navigationOffline);
  const [showUserProfil, setShowUserProfil] = useState(false)

  const navbarUpdate = () => {
    (sessionStorage.getItem('JWT')) ?
      (axios.get(url, {
        headers: {
          'Authorization': sessionStorage.getItem('JWT')
        }
      }).then(
        res => {
          console.log("resultat ", res.data);
          setRole(res.data)
        },
      ).catch(err => {
        console.error(err);
      }
      )) : console.log("déconnecté");
  }

  useEffect(() => {
    console.log("useEffect");
    navbarUpdate()
    login()
  })


  const logout = () => {
    setNavigation(navigationOffline)
    setShowUserProfil(false)
    sessionStorage.clear()
    setRole("")
    history.push('/')
  }

  const login = () => {
    if (role == "ROLE_USER") {
      setNavigation(navigationUtilisateur)
      setShowUserProfil(true)
    } else if (role == "ROLE_BRICOLEUR") {
      setNavigation(navigationBricoleur)
      setShowUserProfil(true)
    } else {
      setNavigation(navigationOffline)
      setShowUserProfil(false)
    }
  }

  return (
    <>
      <Disclosure as="nav" className="bg-gray-800 navbar">
        {({ open }) => (
          <>
            {console.log("mon role", role)}
            <div>
              <div className="relative flex items-center h-16">
                <div className="absolute inset-y-0 left-0 flex items-center md:hidden">
                  {/* Mobile menu button*/}
                  <Disclosure.Button className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                    <span className="sr-only">Open main menu</span>
                    {open ? (
                      <XIcon className="block h-6 w-6" aria-hidden="true" />
                    ) : (
                      <MenuIcon className="block h-6 w-6" aria-hidden="true" />
                    )}
                  </Disclosure.Button>
                </div>
                <div className="flex-1 flex items-center justify-center md:items-stretch md:justify-start">
                  <div onClick={() => history.push("/")}><LogoBricoPote /></div>

                  <div className="hidden md:block md:ml-16">
                    <div className="flex">
                      {navigation.map((item) => (
                        <div onClick={() => history.push(item.route)}
                          key={item.name}
                          className="text-gray-300 font-bold text-lg hover:text-white navbar-item px-3 py-2">
                          {item.name}
                        </div>
                      ))}

                    </div>
                  </div>
                </div>
                <div className="pr-16">
                  {showUserProfil ? <UserProfil logout={logout} role={role} /> :

                    <div className="text-white hidden md:flex">
                      <div onClick={() => history.push('/inscription/1')}
                        className="text-gray-300 font-bold text-lg hover:text-white navbar-item px-3 py-2">
                        Inscription
                      </div>
                      <div onClick={() => history.push('/login')}
                        className="text-gray-300 font-bold text-lg hover:text-white navbar-item px-3 py-2">
                        Connexion
                      </div>
                    </div>}
                </div>

              </div>
            </div>

            <Disclosure.Panel className="md:hidden">
              <div className="px-2 pt-2 pb-3 space-y-1">
                {navigation.map((item) => (
                  <Link
                    key={item.name}
                    to={item.route}
                    className="navbar-item font-bold text-gray-300 hover:text-white block px-3 py-2"
                  >
                    {item.name}
                  </Link >
                ))}
                {navigationConnexion.map((item) => (
                  <Link
                    key={item.name}
                    to={item.route}
                    className="navbar-item font-bold text-gray-300 hover:text-white block px-3 py-2"
                  >
                    {item.name}
                  </Link >
                ))}
                {
                  (role == "ROLE_BRICOLEUR") && navigationDashboard.map((item) => (
                    <div onClick={() => history.push(item.route)}
                      key={item.name}
                      className="text-gray-300 font-bold text-lg hover:text-white navbar-item px-3 py-2">
                      {item.name}
                    </div>
                  ))
                }
              </div>
            </Disclosure.Panel>
          </>
        )}
      </Disclosure>
    </>

  )
}
export default Navbar
