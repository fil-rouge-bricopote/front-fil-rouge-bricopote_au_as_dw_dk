import React from 'react'
import logoBricoPote from '../../Logo/LogoBricopote.svg'

const LogoBricoPote = () => {
    return <img src={logoBricoPote} alt="logo Bricopote" />
}

export default LogoBricoPote
