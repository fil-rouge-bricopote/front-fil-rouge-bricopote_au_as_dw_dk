import React, { Fragment } from 'react'
import pqBricopote from '../../Logo/logoCards2/pqBricopote.svg'
import devBricopote from '../../Logo/logoCards2/devBricopote.svg'
import './InfoBricopoteCard.css'

const InfoBricopoteCard = ({titre,image,texte}) => {

    const afficheImage = (image) => {

        switch (image) {
            case "pqBricopote":
                return <div className="flex justify-center"><img className="w-32" src={pqBricopote}/></div>
                break;
            case "devBricopote":
                return <div className="flex justify-center"><img className="w-32" src={devBricopote}/></div>
                break;
        }
    }

    return (
        <Fragment>
            <div className="w-7/12 md:w-5/12 h-62 flex flex-col bg-white my-8 shadow-xl rounded-xl m-auto">
            <div className="title-gradiant text-xl font-bold p-4 rounded-lg rounded-b-none">{titre}</div>
            {afficheImage(image)}
            <div className="text-xl font-bold mb-8 mx-2">{texte}</div>
            </div>
        </Fragment>
        
    )
}

export default InfoBricopoteCard
