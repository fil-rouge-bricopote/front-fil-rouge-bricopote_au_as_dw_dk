const listeCompetencesSample = [
    {
        "idSousCategorie": 13,
        "intitule": "Jardenerie",
        "codeSousCategorie": "JAR",
        "categorie": {
            "idCategorie": 3,
            "intitule": "Jardinage",
            "codeCategorie": "JAR"
        }
    },
    {
        "idSousCategorie": 14,
        "intitule": "Plante et semence",
        "codeSousCategorie": "PLS",
        "categorie": {
            "idCategorie": 3,
            "intitule": "Jardinage",
            "codeCategorie": "JAR"
        }
    },
    {
        "idSousCategorie": 15,
        "intitule": "Portail, grillage et occultation",
        "codeSousCategorie": "PGO",
        "categorie": {
            "idCategorie": 3,
            "intitule": "Jardinage",
            "codeCategorie": "JAR"
        }
    },
    {
        "idSousCategorie": 16,
        "intitule": "Serre,abri et garage",
        "codeSousCategorie": "SAG",
        "categorie": {
            "idCategorie": 3,
            "intitule": "Jardinage",
            "codeCategorie": "JAR"
        }
    },
    {
        "idSousCategorie": 17,
        "intitule": "Entretien jardin",
        "codeSousCategorie": "ENJ",
        "categorie": {
            "idCategorie": 3,
            "intitule": "Jardinage",
            "codeCategorie": "JAR"
        }
    },
    {
        "idSousCategorie": 18,
        "intitule": "Aménagement jardin",
        "codeSousCategorie": "AMJ",
        "categorie": {
            "idCategorie": 3,
            "intitule": "Jardinage",
            "codeCategorie": "JAR"
        }
    },
    {
        "idSousCategorie": 19,
        "intitule": "Aménagement exterieur",
        "codeSousCategorie": "AME",
        "categorie": {
            "idCategorie": 3,
            "intitule": "Jardinage",
            "codeCategorie": "JAR"
        }
    },
    {
        "idSousCategorie": 20,
        "intitule": "Remise en état du sol",
        "codeSousCategorie": "RES",
        "categorie": {
            "idCategorie": 3,
            "intitule": "Jardinage",
            "codeCategorie": "JAR"
        }
    },
    {
        "idSousCategorie": 21,
        "intitule": "Tonte de la pelouse",
        "codeSousCategorie": "TON",
        "categorie": {
            "idCategorie": 3,
            "intitule": "Jardinage",
            "codeCategorie": "JAR"
        }
    },
    {
        "idSousCategorie": 36,
        "intitule": "Plombier Confirmé",
        "codeSousCategorie": "PLC",
        "categorie": {
            "idCategorie": 6,
            "intitule": "Plomberie",
            "codeCategorie": "PLO"
        }
    },
    {
        "idSousCategorie": 38,
        "intitule": "Cuisine",
        "codeSousCategorie": "CUI",
        "categorie": {
            "idCategorie": 6,
            "intitule": "Plomberie",
            "codeCategorie": "PLO"
        }
    },
    {
        "idSousCategorie": 39,
        "intitule": "Emballage",
        "codeSousCategorie": "EMB",
        "categorie": {
            "idCategorie": 7,
            "intitule": "Déménagement",
            "codeCategorie": "DEM"
        }
    },
    {
        "idSousCategorie": 46,
        "intitule": "Maintenance et réparation",
        "codeSousCategorie": "MAR",
        "categorie": {
            "idCategorie": 9,
            "intitule": "Informatique",
            "codeCategorie": "INF"
        }
    },
    {
        "idSousCategorie": 47,
        "intitule": "Installation",
        "codeSousCategorie": "INS",
        "categorie": {
            "idCategorie": 9,
            "intitule": "Informatique",
            "codeCategorie": "INF"
        }
    }
]

export default listeCompetencesSample;