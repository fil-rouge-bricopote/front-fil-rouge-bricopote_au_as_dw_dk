import React, { useState } from 'react';   // for create-react-app, etc



const StarRating = ({ value, size = 24, onChange }) => {

    // short trick 
    const stars = Array.from({ length: 5 }, () => '🟊')

    // Internal handle change function
    const handleChange = (value) => {
        onChange(value + 1);
    }

    return (
        <div>
            {stars.map((s, index) => {
                let style = '#909090';
                if (index < value) {
                    style = '#F2BC37';
                }
                return (
                    <span className={"star"}
                        key={index}
                        style={{ color: style, width: size, height: size, fontSize: size }}
                        onClick={() => (onChange) && handleChange(index)}>{s}
                    </span>
                    )}
                )
            }
            {value}
        </div>
    )
}


function StarRatingDemo() {
    // Get the rating from a db if required.
    // The value 3 is just for testing.
    const [rating, setRating] = useState(3);

    const handleChange = (value) => {
        setRating(value);
    }
    return (
        <div>
            <h2>Star Rating Demo</h2>

            <StarRating
                size={120}
                value={rating}
                onChange={handleChange}
            />
        </div>
    )
}
export default StarRating;