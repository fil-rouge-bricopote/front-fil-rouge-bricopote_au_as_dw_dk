import React, { useState } from 'react'
import {CSSTransition} from 'react-transition-group'
import './Modal.css'



const Modal3 = ({ btnText, info }) => {

    const [isOpen, setIsOpen] = useState(false);

    const closeModal = () => {
        setIsOpen(false)
    }

    const openModal = () => {
        setIsOpen(true)
    }

    return (
        <>
            <div className="flex items-center justify-center">
                <button
                    type="button"
                    onClick={openModal}
                    className="px-4 py-2 w-full mt-2 text-sm font-bold uppercase text-white bg-yellowBricopote rounded-md hover:bg-opacity-70 focus:outline-none"
                >
                    {btnText}
                </button>
            </div>
            
            <div className={`modal ${isOpen ? 'show' : ''}`} >
                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title">{btnText}</h4>
                    </div>
                    <div className="modal-body">
                        <p>{info}</p>
                    </div>
                    <div className="modal-footer">
                        <button onClick={closeModal} className="px-4 py-2 w-6/12 mt-2 text-sm font-bold uppercase text-white bg-red-200 rounded-md hover:bg-opacity-70 focus:outline-none">Fermer</button>
                    </div>
                </div>
            </div>

        </>


    )
}
export default Modal3
