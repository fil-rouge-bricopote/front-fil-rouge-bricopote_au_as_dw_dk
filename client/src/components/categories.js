const categories = [
    {
        "idCategorie": 1,
        "intitule": "Bricolage Maison",
        "codeCategorie": "BRM",
        "listSousCategories": [
            {
                "idSousCategorie": 1,
                "intule": "Serrurerie",
                "codeSousCategorie": "SER",
                "categorie": {
                    "idCategorie": 1,
                    "intitule": "Bricolage Maison",
                    "codeCategorie": "BRM"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 2,
                "intule": "Ramonage/poêle/cheminée",
                "codeSousCategorie": "RAM",
                "categorie": {
                    "idCategorie": 1,
                    "intitule": "Bricolage Maison",
                    "codeCategorie": "BRM"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 3,
                "intule": "Pose ou réparation store/fenêtre/porte",
                "codeSousCategorie": "POS",
                "categorie": {
                    "idCategorie": 1,
                    "intitule": "Bricolage Maison",
                    "codeCategorie": "BRM"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 4,
                "intule": "Murs/cloisons/revêtement mural/revêtement de sol",
                "codeSousCategorie": "MUR",
                "categorie": {
                    "idCategorie": 1,
                    "intitule": "Bricolage Maison",
                    "codeCategorie": "BRM"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 5,
                "intule": "Tapisserie",
                "codeSousCategorie": "TAP",
                "categorie": {
                    "idCategorie": 1,
                    "intitule": "Bricolage Maison",
                    "codeCategorie": "BRM"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 6,
                "intule": "Accroche murale",
                "codeSousCategorie": "ACM",
                "categorie": {
                    "idCategorie": 1,
                    "intitule": "Bricolage Maison",
                    "codeCategorie": "BRM"
                },
                "bricoleurs": [],
                "annonces": []
            }
        ]
    },
    {
        "idCategorie": 2,
        "intitule": "Peinture",
        "codeCategorie": "PEI",
        "listSousCategories": [
            {
                "idSousCategorie": 7,
                "intule": "Peinture de toit",
                "codeSousCategorie": "PTO",
                "categorie": {
                    "idCategorie": 2,
                    "intitule": "Peinture",
                    "codeCategorie": "PEI"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 8,
                "intule": "Peinture de meubles",
                "codeSousCategorie": "PME",
                "categorie": {
                    "idCategorie": 2,
                    "intitule": "Peinture",
                    "codeCategorie": "PEI"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 9,
                "intule": "Peinture de sol/plafond",
                "codeSousCategorie": "PSP",
                "categorie": {
                    "idCategorie": 2,
                    "intitule": "Peinture",
                    "codeCategorie": "PEI"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 10,
                "intule": "Peinture mur",
                "codeSousCategorie": "PMU",
                "categorie": {
                    "idCategorie": 2,
                    "intitule": "Peinture",
                    "codeCategorie": "PEI"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 11,
                "intule": "Rénovation de sol",
                "codeSousCategorie": "RSO",
                "categorie": {
                    "idCategorie": 2,
                    "intitule": "Peinture",
                    "codeCategorie": "PEI"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 12,
                "intule": "Rénovation mur",
                "codeSousCategorie": "RMU",
                "categorie": {
                    "idCategorie": 2,
                    "intitule": "Peinture",
                    "codeCategorie": "PEI"
                },
                "bricoleurs": [],
                "annonces": []
            }
        ]
    },
    {
        "idCategorie": 3,
        "intitule": "Jardinage",
        "codeCategorie": "JAR",
        "listSousCategories": [
            {
                "idSousCategorie": 13,
                "intule": "Jardenerie",
                "codeSousCategorie": "JAR",
                "categorie": {
                    "idCategorie": 3,
                    "intitule": "Jardinage",
                    "codeCategorie": "JAR"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 14,
                "intule": "Plante et semence",
                "codeSousCategorie": "PLS",
                "categorie": {
                    "idCategorie": 3,
                    "intitule": "Jardinage",
                    "codeCategorie": "JAR"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 15,
                "intule": "Portail, grillage et occultation",
                "codeSousCategorie": "PGO",
                "categorie": {
                    "idCategorie": 3,
                    "intitule": "Jardinage",
                    "codeCategorie": "JAR"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 16,
                "intule": "Serre,abri et garage",
                "codeSousCategorie": "SAG",
                "categorie": {
                    "idCategorie": 3,
                    "intitule": "Jardinage",
                    "codeCategorie": "JAR"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 17,
                "intule": "Entretien jardin",
                "codeSousCategorie": "ENJ",
                "categorie": {
                    "idCategorie": 3,
                    "intitule": "Jardinage",
                    "codeCategorie": "JAR"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 18,
                "intule": "Aménagement jardin",
                "codeSousCategorie": "AMJ",
                "categorie": {
                    "idCategorie": 3,
                    "intitule": "Jardinage",
                    "codeCategorie": "JAR"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 19,
                "intule": "Aménagement exterieur",
                "codeSousCategorie": "AME",
                "categorie": {
                    "idCategorie": 3,
                    "intitule": "Jardinage",
                    "codeCategorie": "JAR"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 20,
                "intule": "Remise en état du sol",
                "codeSousCategorie": "RES",
                "categorie": {
                    "idCategorie": 3,
                    "intitule": "Jardinage",
                    "codeCategorie": "JAR"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 21,
                "intule": "Tonte de la pelouse",
                "codeSousCategorie": "TON",
                "categorie": {
                    "idCategorie": 3,
                    "intitule": "Jardinage",
                    "codeCategorie": "JAR"
                },
                "bricoleurs": [],
                "annonces": []
            }
        ]
    },
    {
        "idCategorie": 4,
        "intitule": "électricité",
        "codeCategorie": "ELE",
        "listSousCategories": [
            {
                "idSousCategorie": 22,
                "intule": "Chauffage et climatisation",
                "codeSousCategorie": "CHC",
                "categorie": {
                    "idCategorie": 4,
                    "intitule": "électricité",
                    "codeCategorie": "ELE"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 23,
                "intule": "Gaz",
                "codeSousCategorie": "GAZ",
                "categorie": {
                    "idCategorie": 4,
                    "intitule": "électricité",
                    "codeCategorie": "ELE"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 24,
                "intule": "Energie renouvelable/verte",
                "codeSousCategorie": "ENR",
                "categorie": {
                    "idCategorie": 4,
                    "intitule": "électricité",
                    "codeCategorie": "ELE"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 25,
                "intule": "Installation luminaire",
                "codeSousCategorie": "INL",
                "categorie": {
                    "idCategorie": 4,
                    "intitule": "électricité",
                    "codeCategorie": "ELE"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 26,
                "intule": "Installation électrique",
                "codeSousCategorie": "INE",
                "categorie": {
                    "idCategorie": 4,
                    "intitule": "électricité",
                    "codeCategorie": "ELE"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 27,
                "intule": "Domotique",
                "codeSousCategorie": "DOM",
                "categorie": {
                    "idCategorie": 4,
                    "intitule": "électricité",
                    "codeCategorie": "ELE"
                },
                "bricoleurs": [],
                "annonces": []
            }
        ]
    },
    {
        "idCategorie": 5,
        "intitule": "Electroménager",
        "codeCategorie": "ELM",
        "listSousCategories": [
            {
                "idSousCategorie": 28,
                "intule": "Réparation/installation de petit éléctroménager",
                "codeSousCategorie": "RIE",
                "categorie": {
                    "idCategorie": 5,
                    "intitule": "Electroménager",
                    "codeCategorie": "ELM"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 29,
                "intule": "Réparation/installation de lave-vaisselle",
                "codeSousCategorie": "RLV",
                "categorie": {
                    "idCategorie": 5,
                    "intitule": "Electroménager",
                    "codeCategorie": "ELM"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 30,
                "intule": "Réparation/installation de lave-linge",
                "codeSousCategorie": "RLL",
                "categorie": {
                    "idCategorie": 5,
                    "intitule": "Electroménager",
                    "codeCategorie": "ELM"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 31,
                "intule": "Réparation/installation de four",
                "codeSousCategorie": "RFO",
                "categorie": {
                    "idCategorie": 5,
                    "intitule": "Electroménager",
                    "codeCategorie": "ELM"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 32,
                "intule": "Réparation/installation de seche-linge",
                "codeSousCategorie": "RSL",
                "categorie": {
                    "idCategorie": 5,
                    "intitule": "Electroménager",
                    "codeCategorie": "ELM"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 33,
                "intule": "Réparation/installation de réfrigérateur",
                "codeSousCategorie": "RRE",
                "categorie": {
                    "idCategorie": 5,
                    "intitule": "Electroménager",
                    "codeCategorie": "ELM"
                },
                "bricoleurs": [],
                "annonces": []
            }
        ]
    },
    {
        "idCategorie": 6,
        "intitule": "Plomberie",
        "codeCategorie": "PLO",
        "listSousCategories": [
            {
                "idSousCategorie": 34,
                "intule": "Robinetterie",
                "codeSousCategorie": "ROB",
                "categorie": {
                    "idCategorie": 6,
                    "intitule": "Plomberie",
                    "codeCategorie": "PLO"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 35,
                "intule": "Petite Plomberie",
                "codeSousCategorie": "PPL",
                "categorie": {
                    "idCategorie": 6,
                    "intitule": "Plomberie",
                    "codeCategorie": "PLO"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 36,
                "intule": "Plombier Conformé",
                "codeSousCategorie": "PLC",
                "categorie": {
                    "idCategorie": 6,
                    "intitule": "Plomberie",
                    "codeCategorie": "PLO"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 37,
                "intule": "Salle de bain",
                "codeSousCategorie": "SDB",
                "categorie": {
                    "idCategorie": 6,
                    "intitule": "Plomberie",
                    "codeCategorie": "PLO"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 38,
                "intule": "Cuisine",
                "codeSousCategorie": "CUI",
                "categorie": {
                    "idCategorie": 6,
                    "intitule": "Plomberie",
                    "codeCategorie": "PLO"
                },
                "bricoleurs": [],
                "annonces": []
            }
        ]
    },
    {
        "idCategorie": 7,
        "intitule": "Déménagement",
        "codeCategorie": "DEM",
        "listSousCategories": [
            {
                "idSousCategorie": 39,
                "intule": "Emballage",
                "codeSousCategorie": "EMB",
                "categorie": {
                    "idCategorie": 7,
                    "intitule": "Déménagement",
                    "codeCategorie": "DEM"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 40,
                "intule": "Chargement/Déchargement",
                "codeSousCategorie": "CHD",
                "categorie": {
                    "idCategorie": 7,
                    "intitule": "Déménagement",
                    "codeCategorie": "DEM"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 41,
                "intule": "Transport",
                "codeSousCategorie": "TRA",
                "categorie": {
                    "idCategorie": 7,
                    "intitule": "Déménagement",
                    "codeCategorie": "DEM"
                },
                "bricoleurs": [],
                "annonces": []
            }
        ]
    },
    {
        "idCategorie": 8,
        "intitule": "Montage de meubles",
        "codeCategorie": "MON",
        "listSousCategories": [
            {
                "idSousCategorie": 42,
                "intule": "Conception de meubles sur-mesure",
                "codeSousCategorie": "CMS",
                "categorie": {
                    "idCategorie": 8,
                    "intitule": "Montage de meubles",
                    "codeCategorie": "MON"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 43,
                "intule": "Installation objet de déco",
                "codeSousCategorie": "IOD",
                "categorie": {
                    "idCategorie": 8,
                    "intitule": "Montage de meubles",
                    "codeCategorie": "MON"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 44,
                "intule": "Montage/Démontage de meubles",
                "codeSousCategorie": "MON",
                "categorie": {
                    "idCategorie": 8,
                    "intitule": "Montage de meubles",
                    "codeCategorie": "MON"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 45,
                "intule": "Montage/Démontage de meubles IKEA",
                "codeSousCategorie": "MIK",
                "categorie": {
                    "idCategorie": 8,
                    "intitule": "Montage de meubles",
                    "codeCategorie": "MON"
                },
                "bricoleurs": [],
                "annonces": []
            }
        ]
    },
    {
        "idCategorie": 9,
        "intitule": "Informatique",
        "codeCategorie": "INF",
        "listSousCategories": [
            {
                "idSousCategorie": 46,
                "intule": "Maintenance et réparation",
                "codeSousCategorie": "MAR",
                "categorie": {
                    "idCategorie": 9,
                    "intitule": "Informatique",
                    "codeCategorie": "INF"
                },
                "bricoleurs": [],
                "annonces": []
            },
            {
                "idSousCategorie": 47,
                "intule": "Installation",
                "codeSousCategorie": "INS",
                "categorie": {
                    "idCategorie": 9,
                    "intitule": "Informatique",
                    "codeCategorie": "INF"
                },
                "bricoleurs": [],
                "annonces": []
            }
        ]
    }
]
export default categories;