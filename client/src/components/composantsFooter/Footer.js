import React from 'react'
import LogoBricoPote from '../composantsNavbar/LogoBricoPote'
import { Link } from 'react-router-dom'
import { useHistory } from "react-router-dom";



const footerLinkList = [
    {name :"A Propos"},
    {name :"Qui sommes nous ?"},
    {name :"Mentions Légales"},
    {name :"Partenaire ?"},
    {name :"Contactez-nous"}
]
const footerLinkList2 = [
    {name :"Devenir Bricopote", route:"/inscription/0"},
    {name :"Inscription", route:"/inscription/1"},
    {name :"Mes Annonces"},
    {name :"DashBord"},
    {name :"Avantages Bricopote"}
]
const footerLinkList3 = [
    {name :"Facebook"},
    {name :"Twitter"},
    {name :"Linkedin"},
    {name :"Instagram"}
]


const Footer = () => {

    let history = useHistory();

    return (
        <>
            <footer className="footer mb-auto bg-gray-800 pt-1 ">
                <div className="container mx-auto px-6">



                    <div class="sm:flex sm:mt-8">
                        <div class="mt-8 sm:mt-0 sm:w-full sm:px-8 flex flex-col md:flex-row justify-between">
                            <div className="m-auto md:mx-0">
                                <div onClick={() => history.push("/")}><LogoBricoPote /></div>
                                <div className="border-b-2 border-white pb-1 text-white">contact@bricopote.fr</div>
                            </div>
                            <div class="flex flex-col">
                                {footerLinkList.map( (item , index) => {
                                    return <span key={index} className="my-2 text-gray-400 hover:text-gray-200">{item.name}</span>
                                }

                                )}
                            </div>
                            <div class="flex flex-col">
                                {footerLinkList2.map( (item) => {
                                    return <div onClick={() => (item.route) && history.push(item.route)} className="my-2 text-gray-400 hover:text-gray-200">{item.name}</div>
                                }

                                )}
                            </div>
                            <div class="flex flex-col">
                                {footerLinkList3.map( (item) => {
                                    return <span className="my-2 text-gray-400 hover:text-gray-200">{item.name}</span>
                                }

                                )}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container mx-auto px-6">
                    <div class="mt-16 border-t-2 border-gray-300 flex flex-col items-center">
                        <div class="sm:w-2/3 text-center py-6">
                            <p class="text-sm text-gray-400 font-bold mb-2">
                                © BricoPote 2021
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </>
    )
}

export default Footer
