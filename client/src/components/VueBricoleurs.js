import React from 'react'
import CatCards from './composantsGrilleCategory/CatCards';
import { Link } from "react-router-dom";

const VueBricoleurs = props => {
    console.log("cat",props.location.infoCard);
    return (
        <div>
            <Link to="/">Acceuil</Link>
            <CatCards cat={props.location.infoCard}/>
            sélection des sous-categories
            <select name="" id="">
                <option>--- Sélectionnez une sous-categorie ---</option>
                {props.location.infoCard.listSousCategories.map((scat, index) => 
                <option>{scat.intitule}</option>)}
            </select>
        </div>
    )
}

export default VueBricoleurs
