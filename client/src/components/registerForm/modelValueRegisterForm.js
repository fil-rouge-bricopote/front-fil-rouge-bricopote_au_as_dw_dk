export default{

    'pwd': '',
    'confirmPassword' :'',
    'email': '',
    'user': {
        'nom': '',
        'prenom': '',
        'tel': '',
        'email': '',
        'dateNaissance': '',
        'adresse': {
            'numero': '',
            'libelle': '',
            'cp': '',
            'ville': ''
        },
        "competences": []
    }

}