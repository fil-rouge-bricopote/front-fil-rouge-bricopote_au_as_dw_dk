import React, { useState } from 'react';
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import SelectionCategorie from '../composantsChoisirCompetences/SelectionCategorie';
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import modelValueRegisterForm from './modelValueRegisterForm';
import UserForm from './userForm';
import axios from "axios"


const inputStyle =
    "py-2 px-3 rounded-lg border-2 border-yellow-600 focus:outline-none focus:ring-2 focus:ring-offset-yellow-600 focus:border-transparent";
const labelStyle =
    "uppercase md:text-sm text-xs text-gray-500 text-light font-semibold";

const RegisterForm = (props) => {

    let errosCompetence = false;
    const step = ['Quel est votre profil ?', 'Inscription', 'Sélectionnez vos compétences !!']
    const [steps, setSteps] = useState(1);
    const [listCompetence, setlistCompetence] = useState([]);
    const [imgFile, setImgFile] = useState(null)
    const [bricoleur, setBricoleur] = useState(false);

    const history = useHistory();

    const backbtnStyle = 'w-auto bg-gray-500 hover:bg-gray-700 rounded-lg shadow-xl font-medium text-white px-4 py-2';

    function nextStep() {
        setSteps(steps => steps + 1)
    }
    function prevStep() {

        if (steps === 2) { setBricoleur(bricoleur => false) }

        setSteps(steps => steps - 1)


    }

    const toShort = ' doit faire plus de 2 caractères';
    const toLong = 'doit faire moins de 25 caractères';
    const obligatoire = 'Le champs est obligatoire !! ';
    const validationSchema = Yup.object().shape({

        email: Yup.string()
            .required(obligatoire)
            .email('Email invalide'),

        pwd: Yup.string()
            .required(obligatoire)
            .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/, "Minimum huit caractères, au moins une lettre majuscule, une lettre minuscule, un chiffre et un caractère spécial")
        ,
        confirmPassword: Yup.string()
            .required(obligatoire)
            .oneOf([Yup.ref('pwd'), null], 'Les mots de passe doivent être identique'),
        user: Yup.object().shape({

            nom: Yup.string()
                .min(2, toShort)
                .max(25, toLong)
                .required(obligatoire),

            prenom: Yup.string()
                .min(2, toShort)
                .max(25, toLong)
                .required(obligatoire),

            tel: Yup.string()
                .matches(/(0[1-9][0-9]{8})/, 'Le numéro de télèphone doit être correct')
                .required(obligatoire),

            dateNaissance: Yup.date().required(obligatoire),

            adresse: Yup.object().shape({

                numero: Yup.string().required(obligatoire),
                libelle: Yup.string().required(obligatoire),
                cp: Yup.string().required(obligatoire).max(6, 'doit faire moins de 6 chiffre'),
                ville: Yup.string().required(obligatoire)

            })

        })
    })

    const stepper = steps => {
        console.log(steps)
        console.log(bricoleur)
        console.log(step.length)
        switch (steps) {
            case 1:
                return (
                    <>
                    <div className='p-2 mt-3 grid gap-3 grid-cols-2 '>
                        <figure className="transition duration-300 ease-in-out transform hover:scale-105 cursor-pointer shadow-xl border-2 border-opacity-30">
                            <img className=" object-cover"  onClick={() => { setBricoleur(bricoleur => true); nextStep() }} src="http://127.0.0.1:3000/img/430.jpg" alt="" /> 
                            <figcaption class="font-bold text-sm  lg:text-xl text-center -mt-5 xl:-mt-10  text-gray-900 px-4">
                                <h1>Devenir Bricopote</h1>
                            </figcaption>
                        </figure>
                       
                       <figure className="transition duration-300 ease-in-out transform hover:scale-105 cursor-pointer shadow-xl border-2 border-opacity-30">   
                            <img className=" object-cover " onClick={() => { nextStep() }} src="http://127.0.0.1:3000/img/3071357.jpg" alt="" />
                            <figcaption class="font-bold  text-sm lg:text-xl text-center -mt-5 xl:-mt-10  text-gray-900 px-4">
                                <h1>Simple Utilisateur</h1>
                            </figcaption>
                        </figure>
                    </div>
                        
                    </>
                );

            case 2:
                return (
                    <>
                        <div className="grid md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                            <UserForm />

                            <div className="grid grid-cols-1 ">
                                <label className={labelStyle}>
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        class="inline-flex flex-shrink-0 w-6 h-6 -mt-1 mr-1"
                                        viewBox="0 0 24 24" stroke-width="2"
                                        stroke="currentColor" fill="none"
                                        stroke-linecap="round"
                                        stroke-linejoin="round">
                                        <path d="M5 7h1a2 2 0 0 0 2 -2a1 1 0 0 1 1 -1h6a1 1 0 0 1 1 1a2 2 0 0 0 2 2h1a2 2 0 0 1 2 2v9a2 2 0 0 1 -2 2h-14a2 2 0 0 1 -2 -2v-9a2 2 0 0 1 2 -2" />
                                        <circle cx="12" cy="13" r="3" />
                                    </svg>
                                    Image de profils
                                </label>
                                <label htmlFor="fileInput" className={inputStyle}>

                                    <input name="imageProfil" id="fileInput" accept="image/*"
                                        onChange={(event) => { setImgFile(event.currentTarget.files[0]) }} type="file" />

                                </label>
                            </div>
                        </div>
                    </>)

            case 3:
                return (<SelectionCategorie listCompetence={listCompetence} setlistCompetence={setlistCompetence} />)
            default:
                break;
        }
    }

    const onSubmit = values => {

        let formData = new FormData();

        values.user.competences = listCompetence;
        values.user.email = values.email;

        delete values.confirmPassword;
        console.log(imgFile);

        formData.append('img', imgFile);
        formData.append('auth', JSON.stringify(values));


        console.log(formData.get('auth'))

        console.log(formData.get('img'))

        if (!bricoleur) {


            axios.post("http://127.0.0.1:5000/api/signup/u", formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then(response => {
                console.log(response);
            })
                .catch(function (erreur) {
                    console.log(erreur);
                });
            history.push('/')

        } else if (values.user.competences.length < 1) {

            errosCompetence = true;
            console.log(errosCompetence)

        } else {

            axios.post("http://127.0.0.1:5000/api/signup/b", formData).then(response => {
                console.log(response);
            })
                .catch(function (erreur) {
                    console.log(erreur);
                });
            history.push('/')
        }

    }



    return (
        <>
            <Formik
                initialValues={modelValueRegisterForm}
                onSubmit={steps < step.length && bricoleur ? nextStep : onSubmit}
                validationSchema={validationSchema}
            >
                <div className=" flex bg-gray-200 items-center justify-center ">
                    <div className=" grid bg-white  rounded-lg shadow-xl w-11/12 md:w-9/12 lg:w-1/2  mt-16 mb-16">
                        <Form  >

                            <div className="flex justify-center">
                                <div className="flex">
                                    <h1 className="text-gray-600 font-bold md:text-2xl text-xl mt-7">{step[steps - 1]}</h1>
                                </div>
                            </div>

                            {stepper(steps)}

                            {listCompetence.length < 1 && steps === step.length ? <div className="text-red-600 text-center font-semibold">
                                Veuillez selectionez au moins une compétence
                            </div> : null}

                            <div className='flex items-center justify-center  md:gap-8 gap-4 pt-5 pb-5'>
                                {steps > 1 ?
                                    <input type="button" value={"Précendent"} onClick={prevStep} className={backbtnStyle} /> : <Link to='/'><button className={backbtnStyle}>Retour</button></Link>
                                }
                                {steps > 1 ? 
                                    <input type="submit" value={steps === step.length || !bricoleur ? "Valider" : "Suivant"} className='w-auto bg-green-600 hover:bg-green-800 rounded-lg shadow-xl font-medium text-white px-4 py-2' />
                                    : null
                                 
                                }
                            </div>
                        </Form>
                    </div>
                </div>

            </Formik >

        </>
    )


}

export default RegisterForm;