import React, { useState } from "react";
import { EyeIcon } from '@heroicons/react/solid';
import { EyeOffIcon } from '@heroicons/react/solid';



const PasswordInput = ({field, form }) => {
    const [showHidePassword, changeShowHidePassword] = useState(false);

    const inputStyle =
        "py-2 px-3 rounded-lg border-2 border-yellow-600 focus:outline-none focus:ring-2 focus:ring-offset-yellow-600 focus:border-transparent";
    const labelStyle =
        "uppercase md:text-sm text-xs text-gray-500 text-light font-semibold";


    return (

        <>
            <label className={labelStyle}>
                {field.name === "pwd" ? "Mot de passe " : "Confirmation mot de passe"}
                <i onClick={() => changeShowHidePassword(!showHidePassword)}>
                    {showHidePassword ? <EyeOffIcon className=" inline-flex -mt-2 ml-2 h-5 w-5"/> : <EyeIcon className=" inline-flex -mt-2 ml-2 h-5 w-5"/>}</i>
            </label>
            <input className={inputStyle} type={showHidePassword ? "text" : "password"}  {...field} />
        
        </>

    );
};

export default PasswordInput;