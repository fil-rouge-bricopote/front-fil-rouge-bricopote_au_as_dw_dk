import React, { useState } from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import * as Yup from 'yup';
import SousCategoriesDropDownList from './SousCategoriesDropDownList';
import TextErreur from '../Errors/TextErreur'
import axios from 'axios';


const FormAddAnnonce = () => {

    const [cat, setCat] = useState([])

    const inputStyle = "col-span-3 border-2 border-yellowBricopote rounded-lg"
    const btnStyle = " col-start-3 py-2 px-3 rounded-lg bg-yellowBricopote focus:outline-none focus:ring-2 focus:ring-offset-yellow-600 focus:border-transparent"
    const catStyle = "mx-4  mt-2 col-span-3 border-2 border-yellowBricopote rounded-lg"

    const initialValues = {
        titre: '',
        contenu: '',
        localisation: '',
        tarif: '',
        sousCategorie: null
    }

    const onSubmit = values => {
        console.log("Form data", values);
        axios.get("http://localhost:8080/api/public/souscategorie/SER").then()
        values.sousCategorie = 
        console.log(cat)
    }

    const validationSchema = Yup.object({
        titre: Yup.string().required('Requis'),
        contenu: Yup.string().required('Requis'),
        localisation: Yup.string().required('Requis'),
        
    })



    return (

        <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={onSubmit}
        >
            <div className="w-full flex items-center justify-center bg-gray-200 ">
                <div className=" bg-white  rounded-lg shadow-2xl w-11/12 md:w-9/12 lg:w-1/2  mt-16 mb-16">
                    <div className="flex justify-center">
                        <div className="flex">
                            <h1 className="text-gray-600 font-bold md:text-2xl text-xl">Poster une annonce</h1>
                        </div>
                    </div>
                    <Form>
                        {/* <SousCategoriesDropDownList/> */}
                        <div className="p-4 grid grid-cols-4">
                                <p className="w-auto">titre :</p>
                                <Field className={inputStyle}
                                    type='text'
                                    id='titre'
                                    name='titre' />

                                <ErrorMessage component={TextErreur} name='titre' />
                                
                        
                                <p>message de votre annonce</p>
                                <Field className={inputStyle}
                                    as='textarea'
                                    id='contenu'
                                    name='contenu' />

                                <ErrorMessage component={TextErreur} name='contenu' />
                          
                         
                                <p>localisation :</p>
                                <Field className={inputStyle}
                                    type="text"
                                    id='localisation'
                                    name='localisation' />
                                <ErrorMessage component={TextErreur} name='localisation' />
                     

                         
                                <p>tarif : </p>
                                <Field className={inputStyle}
                                    type="text"
                                    id='tarif'
                                    name='tarif' />
                                <ErrorMessage component={TextErreur} name='tarif' />
                       

                                
                                
                            
                            <button type="submit" className={btnStyle}>Envoyer</button>
                        </div>
                    </Form>
                </div>
            </div>
        </Formik >


    )
}

export default FormAddAnnonce
