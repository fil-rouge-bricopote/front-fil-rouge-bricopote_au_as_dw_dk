import React, { useState , useEffect } from 'react'
import ServiceCategory from '../../services/ServiceCategory'

const SousCategoriesDropDownList = () => {

    const [sousCategorie, setSousCategorie] = useState([]);

    useEffect(() => {
        ServiceCategory.showAllSousCategories()
            .then((res) => setSousCategorie(res.data))
            .catch((err) =>
                console.log("ERROR :", err, " Chargement de la liste FAILD")
            );
    }, []);

    const handle = ({target}) => {
        console.log(target);
        setSousCategorie(target.value)
    }


    return (
        <select name="cat" onChange={handle}>
            <option value="">--Catégorie--</option>
            {sousCategorie.map((scat, index) => {
                return (
                    <div key={index}>
                        <option value={scat}>
                            {scat.intitule}
                        </option>

                    </div>)
            })}
        </select>
    )
}

export default SousCategoriesDropDownList
