import {React} from "react";
import { useFormik } from "formik";

export default function CardSettings({user}) {


  const pathDefault = "http://127.0.0.1:3000/img/defaultImg.jpg";
  const pathImg = "http://127.0.0.1:3000/pi/n/";
  
  const initialValues = {
    id : user.idUser,
    nom : user.nom,
    prenom : user.prenom,
    email : user.email,
    tel : user.tel,
    createDate : user.createDate,
    imageProfil : user.imageProfil,
    adresse : {
      idAdresse: user.adresse.idAdresse,
      numero : user.adresse.numero,
      ville : user.adresse.ville,
      libelle : user.adresse.libelle,
      cp : user.adresse.cp
    }
  }

  const onSubmit = (values) => {
   console.log(JSON.stringify(values))
  }

  const formik = useFormik({
    initialValues, 
    onSubmit
  
  })

  return (
    <>
      {console.log("==========================================S",formik.values)}
      <div className="relative flex flex-col  mt-20  min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-100 border-0">
        <div className="w-full lg:w-3/12 mx-auto flex justify-center">
          <div className="relative">
            <div className="w-40 -mt-20">
              <img src={user.imageProfil!= '' && user.imageProfil != null  ?  pathImg+user.imageProfil : pathDefault} alt="..." className=" object-cover rounded-full shadow-xl w-40 h-44 align-middle border-none undefined" />
            </div>
          </div>
        </div>

        <div className="rounded-t bg-white mb-0 px-6 py-6">
          <div className="text-center flex justify-between">

            <h6 className="text-blueGray-700 text-xl font-bold">Mes informations</h6>
        
          </div>
        </div>
        <div className="flex-auto px-4 lg:px-10 py-10 pt-0">
          
          {/* ------------FORMULMAIRE------ */}
          <form onSubmit={formik.handleSubmit}>
          <button
              className="bg-blue-500 text-white active:bg-blue-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 ease-linear transition-all duration-150"
              type="sssubmit"
              
            >
              Modifier
            </button>
            <h6 className="text-blueGray-400 text-sm mt-3 mb-6 font-bold uppercase">
              Identité
            </h6>
            <div className="flex flex-wrap">
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    Nom
                  </label>
                  <input
                    type="text"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    value={formik.values.nom}
                    onChange={formik.handleChange}
                    id="nom"
                    name="nom"
                  />
                </div>
              </div>
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    prénom
                  </label>
                  <input
                    type="text"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    value={formik.values.prenom}
                    onChange={formik.handleChange}
                    id="prenom"
                    name="prenom"
                  />
                </div>
              </div>
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    Email
                  </label>
                  <input
                    type="email"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    id="email"
                    name ="email"
                  />
                </div>
              </div>
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    telephone
                  </label>
                  <input
                    type="text"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    value={formik.values.tel}
                    onChange={formik.handleChange}
                    id="tel"
                    name="tel"
                  />
                </div>
              </div>
            </div>

            <hr className="mt-6 border-b-1 border-blueGray-300" />

            <h6 className="text-blueGray-400 text-sm mt-3 mb-6 font-bold uppercase">
              Contact
            </h6>
            <div className="flex flex-wrap">
              <div className="w-full lg:w-4/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    Numero
                  </label>
                  <input
                    type="text"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    value={formik.values.adresse.numero}
                    onChange={formik.handleChange}
                    id="adresse.numero"
                    name="adresse.numero"
                  />
                </div>
              </div>
              <div className="w-full lg:w-4/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    code postal
                  </label>
                  <input
                    type="text"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    value={formik.values.adresse.cp}
                    onChange={formik.handleChange}
                    id="adresse.cp"
                    name="adresse.cp"
                  />
                </div>
              </div>
              <div className="w-full lg:w-4/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    Ville
                  </label>
                  <input
                    type="text"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    value={formik.values.adresse.ville}
                    onChange={formik.handleChange}
                    id="adresse.ville"
                    name="adresse.ville"
                  />
                </div>
              </div>
              <div className="w-full lg:w-12/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    rue
                  </label>
                  <input
                    type="text"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    value={formik.values.adresse.libelle}
                    onChange={formik.handleChange}
                    id="adresse.libelle"
                    name="adresse.libelle"
                  />
                </div>
              </div>
            </div>
          </form>
          {/* Fin Formulaire */}
        </div>
      </div>
    </>  
  );
}
