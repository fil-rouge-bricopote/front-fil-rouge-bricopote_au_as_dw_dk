import React , {useState , useEffect} from 'react'
import { Switch, Route, useParams, useRouteMatch, Link } from "react-router-dom";
import Messagerie from './Messagerie';
import MonProfil from './MonProfil';
import IconDashboard from "./IconDashboard";
import MesAnnonces from './MesAnnonces';
import axios from 'axios'

const optionButtonsBricoleur = [
    { name: 'Mes Infos', route: '/monProfil', nested: true, iconPath: 'mesInfos' },
    { name: 'Ma messagerie', route: '/messagerie', nested: true, iconPath: 'messagerie' },
    { name: 'Voir mes Annonces', route: '/mesAnnonces', nested: true, iconPath: 'mesAnnonces' },
    { name: 'Rechercher des annonces', nested: false, iconPath: 'rechercherAnnonce' },
    { name: 'Modifier mes competences', route: '/chooseCat', nested: false, iconPath: 'modifierCompetences' }
]

const optionButtonsAdmin = [
    { name: 'Utilisateurs', route: '/monProfil/20' },
    { name: 'Annonces' }
]

const optionButtonsUser = [
    { name: 'Mes Infos', route: '/monProfil', nested: true, iconPath: 'mesInfos' },
    { name: 'Ma messagerie', route: '/messagerie', nested: true, iconPath: 'messagerie' }
]

const Dashboard = props => {
    let { path, url } = useRouteMatch();
    //const [role, setRole] = useState("");
    const role = props.location.state.role
    const [optionButtons, setOptionButtons] = useState([]);

    const getRole = () => {
        // (sessionStorage.getItem('JWT')) &&
        // axios.get(url, {
        //     headers: {
        //         'Authorization': sessionStorage.getItem('JWT')
        //     }
        // }).then(
        //     res => {
        //         console.log("resultat ", res.data);
        //         setRole(res.data)
        //     },
        // ).catch(err => {
        //     console.error(err);
        // }
        // )
    }

    useEffect(() => {
        //getRole()
        customOptionButton()
    })

    const customOptionButton = () => {
        if (role == "ROLE_USER") {
            setOptionButtons(optionButtonsUser)
        } else if (role == "ROLE_BRICOLEUR") {
            setOptionButtons(optionButtonsBricoleur)
        }else{
            setOptionButtons(optionButtonsAdmin)
        }
    }

return (
    <div className="flex justify-center md:justify-start">
        {console.log(role)}
        <nav className="hidden mt-4 md:block md:flex-row md:flex-nowrap shadow-xl bg-white flex-wrap items-center justify-between md:w-80 md:h-full z-10 py-4 px-6">
            <div className="md:pb-40 md:flex-col md:items-stretch md:min-h-full md:flex-nowrap px-0 flex flex-wrap items-center justify-between mx-auto">
                <div>

                    <h6 className="md:min-w-full text-blueGray-500 text-xl uppercase font-bold block pt-1 pb-4 no-underline">
                        Votre dashboard
                    </h6>
                    <hr className="my-4 md:min-w-full" />

                    <ul className="md:flex-col md:min-w-full flex flex-col list-none md:mb-4">

                        {optionButtons.map((item, index) =>

                            <li className="items-center">

                                <Link
                                    className=" flex mt-6 navbar-item  justify-items-center items-center text-blueGray-700 hover:text-blueGray-500 text-lg uppercase py-3"
                                    to={(item.nested) ? `${url}${item.route}` : `${item.route}`}
                                    key={index}
                                >
                                    <IconDashboard codeImage={item.iconPath} />
                                    <p className="ml-4">{item.name}</p>
                                </Link>
                            </li>
                        )}

                    </ul>
                </div>
            </div>
        </nav>
        <Switch>
            <Route exact path={path}>
                <Rubrique />
            </Route>
            <Route path={`${path}/:rubriqueId`}>
                <Rubrique />
            </Route>
        </Switch>
    </div>
)
}

const Rubrique = () => {

    let { rubriqueId } = useParams();

    const toggleRubrique = (rubriqueId) => {
        switch (rubriqueId) {
            case "monProfil":
                return <MonProfil />
            case "messagerie":
                return <Messagerie />
            case "mesAnnonces":
                return <MesAnnonces />
            default:
                return <MonProfil />
        }
    }

    return (
        <div >
            {toggleRubrique(rubriqueId)}
        </div>
    )
}

export default Dashboard
