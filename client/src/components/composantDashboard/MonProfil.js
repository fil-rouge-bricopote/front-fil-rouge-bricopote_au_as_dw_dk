import axios from "axios";
import CardSettings from "./CardSettings";
import { useState, useEffect } from "react";

const MonProfil = () => {

  let token = sessionStorage.getItem("JWT");

  const [user, setUser] = useState();
  
  useEffect(() => {
    axios
    .get("http://127.0.0.1:8080/api/user/info", {
      headers: { Authorization: `Bearer ${token}` },
    })
    .then((res) => {
      setUser(() => res.data);
   
    })
    .catch(function (erreur) {
      console.log(erreur);
    });

  },[])
 

  return user != null ?  (
    
    <>
   
      <div className="w-full md:px-40 pt-8">
        <CardSettings user={user}/>
      </div>
    </>
  ) : ""
};
export default MonProfil;
