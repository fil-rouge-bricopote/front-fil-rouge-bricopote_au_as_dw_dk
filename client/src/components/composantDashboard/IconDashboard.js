import React from 'react'
import mesInfos from './assets/icons/mesInfos.png'
import messagerie from './assets/icons/messagerie.png'
import mesAnnonces from './assets/icons/mesAnnonces.png'
import rechercherAnnonce from './assets/icons/rechercherAnnonce.png'
import voir from './assets/icons/voir.png'
import modifierCompetences from './assets/icons/modifierCompetences.png'

const IconDashboard = ({ codeImage }) => {

    const icon = (codeImage) => {
        switch (codeImage) {
            case "mesInfos":
                return <img src={mesInfos} alt={codeImage} className="w-12"/>

            case "messagerie":
                return <img src={messagerie} alt={codeImage} className="w-12" />

            case "rechercherAnnonce":
                return <img src={rechercherAnnonce} alt={codeImage} className="w-12"/>

            case "mesAnnonces":
                return <img src={mesAnnonces} alt={codeImage} className="w-12"/>

            case "voir":
                return <img src={voir} alt="" className="w-12" />
            
            case "modifierCompetences":
                    return <img src={modifierCompetences} alt="" className="w-12"/>

        }

    }


    return (
        <div>
            {icon(codeImage)}
        </div>
    )
}

export default IconDashboard
