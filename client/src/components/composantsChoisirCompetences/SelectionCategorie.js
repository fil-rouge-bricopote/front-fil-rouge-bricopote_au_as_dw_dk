import React, { useState, useEffect } from 'react'
import CategorieListItem from './CategorieListItem';
import ServiceCategory from '../../services/ServiceCategory';


const SelectionCategorie = ({listCompetence, setlistCompetence}) => {

    const [categories, setCategories] = useState([]);
    const [reload, setReload] = useState(true);
    const [categorieActive , setCategorieActive] = useState(true);


    useEffect(() => {
        ServiceCategory.showAllCategories().then(response => {
            setCategories(response.data);
        })
    }, [reload])

    return (
        <div className="flex-row">
            <div className="flex justify-around">
                <div>
                    <ul>
                        {categories.map((cat, index) => <li key={index} ><CategorieListItem categorieActive={categorieActive} setCategorieActive={setCategorieActive} cat={cat} setlistCompetence={setlistCompetence} listCompetence={listCompetence} /></li>)}
                    </ul>
                </div>
                
            </div>
        </div>

    )
}

export default SelectionCategorie
