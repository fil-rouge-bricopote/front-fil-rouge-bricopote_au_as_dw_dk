import React , {useState, useEffect} from 'react'
import SelectionCategorie from './SelectionCategorie'
import ServiceCategory from '../../services/ServiceCategory'
import InventaireCompetence from './InventaireCompetence';
import listCompetenceSample from '../composantsProfilPage/listeCompetencesSample'

const DashBoardCompetence = props => {

    const [categories, setCategories] = useState([]);
    const [listCompetence, setlistCompetence] = useState(listCompetenceSample)
    const [reload, setReload] = useState(true);

    useEffect(() => {
        ServiceCategory.showAllCategories().then(response => {
            setCategories(response.data);
        })
    }, [reload])

    return (
        <div className="bg-gray-100">
            <h1 className="text-2xl font-bold text-center pt-4">Sélection des compétences</h1>
            <div className=" grid grid-cols-2 m-12 gap-14">
                <div className="bg-white shadow-lg rounded-lg py-4">
                    <SelectionCategorie listCompetence={listCompetence} setlistCompetence={setlistCompetence}/>
                </div>

                <div className="bg-white shadow-lg rounded-lg p-4">
                    <InventaireCompetence sousCatList={listCompetence} categories={categories} />
                </div>
                

                
            </div>
        </div>
    )
}

export default DashBoardCompetence
