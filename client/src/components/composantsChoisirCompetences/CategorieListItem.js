import React, { useState, Fragment } from 'react'
import Collapse from "@kunukn/react-collapse";

const CategorieListItem = ({ cat, categorieActive , setCategorieActive , setlistCompetence , listCompetence }) => {



    const catfieldClick = (catCode) => {
        (catCode == categorieActive)? setCategorieActive("") : setCategorieActive(catCode)
    }


    const addSousCatToList = (scat,e) => {
        const index = listCompetence.indexOf(scat)
        if (e.target.checked) {
            setlistCompetence(prevSousCatList => [...prevSousCatList, scat])
        } else {
            setlistCompetence(listCompetence.filter(scat2 => scat2.intitule !== scat.intitule))
        }
    }

    return (
        <Fragment >
            <div>
                <div onClick={() => catfieldClick(cat.codeCategorie)} className={`w-full h-14 bg-${cat.codeCategorie.toLowerCase()} text-xl font-bold text-gray-100 hover:bg-gray-100 hover:text-${cat.codeCategorie.toLowerCase()}`}>
                    {cat.intitule} <img className="w-8 relative left-80 bottom-4" src={`${process.env.PUBLIC_URL}/img/img-${cat.codeCategorie.toLowerCase()}.png`} />
                </div>
                <Collapse 
                    isOpen={cat.codeCategorie == categorieActive}
                    transition="height 1000ms cubic-bezier(.4, 0, .2, 1)"
                >
                    <ul  >
                        {cat.listSousCategories.map((scat) => 
                        <li className={`w-full bg-${scat.categorie.codeCategorie.toLowerCase()} text-gray-100 border-2 w-80 h-16 justify-between`}>
                            <input 
                                type="checkbox" 
                                name="checkboxSC" 
                                value={scat.codeSousCategorie} 
                                className={`h-8 w-8 rounded-md text-${scat.categorie.codeCategorie.toLowerCase()}`} 
                                onChange={(e) => addSousCatToList(scat,e)} 
                                defaultChecked={
                                    (listCompetence.filter(comp => comp.codeSousCategorie == scat.codeSousCategorie).length > 0)
                                } />   
                                
                                {scat.intitule}
                        </li>)}
                    </ul>
                </Collapse>
            </div>
        </Fragment>
    )
}

export default CategorieListItem
