import React , {useState, useEffect} from 'react'
import GroupeCompetence from './GroupeCompetence';
import ServiceCategory from '../../services/ServiceCategory';

const InventaireCompetence = props => {

    const [categories, setCategories] = useState([]);
    const [reload, setReload] = useState(true);

    useEffect(() => {
        ServiceCategory.showAllCategories().then(response => {
            setCategories(response.data);
        })
    }, [reload])



    return (
        <div className="grid grid-cols-1 ">

            {categories.map((cat,index) => <GroupeCompetence key={index} cat={cat} scats={props.sousCatList} />)}
            
        </div>
    )
}

export default InventaireCompetence
