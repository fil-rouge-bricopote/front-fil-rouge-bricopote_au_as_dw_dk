import React,{useState} from "react";
import { useFormik } from "formik";
import {useHistory} from "react-router-dom";
// import * as yup from "yup";
import axios from "axios";
import fb from "../../img/login/facebook.svg";
import google from "../../img/login/google.svg";

const initialValues = {
  email: "",
  password: "",
};

// const validationSchema = yup.object({
//   email: yup.string().email("Adresse email invalide.").required("Requis."),
//   password: yup.string().required("requis"),
// });

export default function Login(props) {

  const history = useHistory();
  const [incorrect,setIncorrect] = useState(false);
  const onSubmit = (values) => {
    console.log(values);
    axios
      .post("http://127.0.0.1:8080/login", {
        username: values.email,
        password: values.password,
      })
      .then((res) => {
        setIncorrect(false);
        sessionStorage.setItem("JWT", res.headers.authorization);
        history.push('/');
        window.location.reload();
      })
      .catch((err) => {setIncorrect(true)});
  };

  const formik = useFormik({
    initialValues,
    onSubmit,
    // validationSchema,
  });

  return (
    <>
      <main className="">
        <section className="my-20 w-full h-full">
          <div className="container  mx-auto px-4 h-full">
            <div className="flex content-center items-center justify-center h-full">
              <div className="w-full lg:w-4/12 px-4">
                <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-2xl rounded-lg bg-gray-100 border-0">
                  {/* ********************************************************** */}
                  {/* **********SOCIAL MEDIA LOGIN BUTTONS**************/}
                  <div className="rounded-t mb-0 px-6 py-6">
                    <div className="text-center mb-3">
                      <h2 className="text-gray-500 text-lg font-semibold">
                        Se connecter avec
                      </h2>
                    </div>
                    <div className="btn-wrapper text-center">
                      <button
                        className="bg-white active:bg-gray-100 text-gray-700 font-normal px-4 py-2 rounded outline-none focus:outline-none mr-2 mb-1 uppercase shadow hover:shadow-md inline-flex items-center font-semibold text-sm"
                        type="button"
                        style={{ transition: "all .15s ease" }}
                      >
                        <img alt="..." className="w-5 mr-1" src={fb} />
                        FaceBook
                      </button>
                      <button
                        className="bg-white active:bg-gray-100 text-gray-700 font-normal px-4 py-2 rounded outline-none focus:outline-none mr-2 mb-1 uppercase shadow hover:shadow-md inline-flex items-center font-semibold text-sm"
                        type="button"
                        style={{ transition: "all .15s ease" }}
                      >
                        <img alt="..." className="w-5 mr-1" src={google} />
                        Google
                      </button>
                    </div>
                    <hr className="mt-6 border-b-2 rounded-2xl border-gray-300" />
                  </div>
                  {/* ********************************************************** */}

                  {/* CREDENTIALS FIELDS */}
                  <div className="flex-auto px-4 lg:px-10 py-5 pt-0">
                    <div className="text-gray-500 text-center text-md mb-3 font-semibold">
                      <h2>Ou Se connecter avec votre E-mail</h2>
                    </div>
                    <form onSubmit={formik.handleSubmit}>
                      {/* ***************************************************************************************** */}
                      {/* ******************* EMAIL******************** */}
                      <div className="relative w-full mb-3">
                        <label
                          className="block uppercase text-gray-700 text-xs font-bold mb-2"
                          htmlFor="grid-password"
                        >
                          E-mail
                        </label>
                        <input
                          type="email"
                          id="email"
                          required
                          name="email"
                          onChange={formik.handleChange}
                          value={formik.values.email}
                          className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring-yellowBricopote focus:ring-2 w-full"
                          placeholder="Email"
                          style={{ transition: "all .15s ease" }}
                        />
                        {/* <label
                          htmlFor="error-email"
                          className="block uppercase text-red-700 text-xs font-bold mb-2"
                        >
                          {formik.errors.email}
                        </label> */}
                      </div>
                      {/* **************************************************************************************** */}
                      {/***************** PASSWORD *********************/}

                      <div className="relative w-full mb-3">
                        <label
                          className="block uppercase text-gray-700 text-xs font-bold mb-2"
                          htmlFor="grid-password"
                        >
                          Mot de passe
                        </label>
                        <input
                          type="password"
                          required
                          id="password"
                          name="password"
                          onChange={formik.handleChange}
                          value={formik.values.password}
                          className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring-yellowBricopote focus:ring-2 w-full"
                          placeholder="Mot de passe"
                          style={{ transition: "all .15s ease" }}
                        />
                        {/* <label
                          htmlFor="error-password"
                          className="block uppercase text-red-700 text-xs font-bold mb-2"
                        >
                          {formik.errors.password}
                        </label> */}
                      </div>
                      <div className="relative w-full mb-3">
                        {
                          incorrect? (
                          <label
                            htmlFor="error-password"
                            className="block uppercase text-red-700 text-xs font-bold mb-2"
                          >
                            Email ou mot de pass incorecte.
                          </label>):''
                          }
                      </div>
                      {/* **************************************************************************************** */}
                      {/* ***************** SUBMIT BUTTON *****************/}
                      <div className="text-center mt-6">
                        <button
                          className="bg-yellowBricopote text-white active:bg-gray-700 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
                          type="submit"
                          style={{ transition: "all .15s ease" }}
                        >
                          Se connecter
                        </button>
                      </div>
                      {/* ***************************************************************************************** */}
                    </form>
                  </div>
                </div>

                <div className="flex flex-wrap mt-6">
                  {/* ********************************************************* */}
                  {/* ************** FORGET PASSWORD LINK*************** */}
                  <div className="w-1/2">
                    <a
                      href="#pablo"
                      onClick={(e) => e.preventDefault()}
                      className="text-gray-500 hover:text-yellowBricopote"
                    >
                      <small>Mot de passe oublié?</small>
                    </a>
                  </div>
                  {/* ********************************************************* */}
                  {/* *************** SIGN UP LINK************* */}
                  <div className="w-1/2 text-right">
                    <small className="text-gray-400">Pas de compte?</small>
                    <a
                      href="#pablo"
                      onClick={(e) => e.preventDefault()}
                      className="text-gray-500 hover:text-yellowBricopote"
                    >
                      <small> S'inscrire</small>
                    </a>
                  </div>
                  {/* ********************************************************** */}
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </>
  );
}
