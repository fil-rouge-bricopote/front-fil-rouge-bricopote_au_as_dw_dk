import React, {Fragment, useEffect, useState , useContext} from 'react'
import CatCards from './CatCards'
import ServiceCategory from '../../services/ServiceCategory'


const CatGrid = props => {

    const [categories, setCategories] = useState([]);
    const [reload, setReload] = useState(true);

    useEffect(() => {
        ServiceCategory.showAllCategories().then(response => {
            setCategories(response.data);
        })
    }, [reload])

    console.log(props);


    return (

        <div className="w-full flex justify-center">
            <div className="w-full xl:w-7/12 py-8 bg-font grid grid-cols-1 md:grid-cols-2 md:px-36 lg:grid-cols-3 gap-y-3" >
            {categories.map((cat,index) => <CatCards key={index} cat={cat}/>)}
        </div>
        </div>

    )
}

export default CatGrid
