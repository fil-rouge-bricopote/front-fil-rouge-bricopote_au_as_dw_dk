const express = require("express");
const cors = require("cors");
const upload = require("express-fileupload");
const Jimp = require("jimp");
const axios = require("axios");
const { v1: uuidv1 } = require("uuid");

const BACK_API_URL = `http://127.0.0.1:8080/api/public`;
const DIR_IMG_PATH = `${__dirname}/client/public/pi/n`;
const DIR_SM_IMG_PATH = `${__dirname}/client/public/pi/r`;
const PORT = 5000;

/*********************Functions**************************** */

//Pour recuperer l'extention de l'image "jpeg,png"
const getExtention = (type) => {
  const split1 = type.split("/");
  if (split1.length === 2) {
    return split1[1];
  } else {
    return "";
  }
};

//Pour vérifier s'il s'ajit d'une image
const isImage = (file) =>
  file.mimetype === "image/jpeg" || file.mimetype === "image/png";

const getFileInfos = (file) => {
  // Pour recuperer l'extention et s'il s'ajit d'une image dans un objet js
  const check = isImage(file);

  return {
    isImage: check,
    ext: check ? getExtention(file.mimetype) : "",
  };
};

//Pour generer un nom aleatoire
const generateImageId = (ext) => `${uuidv1()}.${ext}`;

//Pour changer la taille d'une image
const resizeImage = (nom) => {
  Jimp.read(`${DIR_IMG_PATH}/${nom}`).then((image) => {
    image.resize(300, Jimp.AUTO);
    image.write(`${DIR_SM_IMG_PATH}/${nom}`);
  });
};

/********************************************************************************/

const sendSignUpInfos = (res, path, data) => {
  axios
    .post(`${BACK_API_URL}/${path}`, data)
    .then(() => res.status(200).json({ msg: "Requette Bien enregistré." }))
    .catch((err) => res.status(500).json({ msg: err }));
};

const signup = (req, res, path) => {
  //Si il ya de contenue textuel envoye
  if (req.body) {
    //Si les info d'utilisateur sont envoyées
    if (req.body.auth) {
      console.log(req.body.auth);
      let auth = JSON.parse(req.body.auth);

      //Si il y a des fichiers
      if (req.files) {
        const file = req.files.img;
        const info = getFileInfos(file);

        if (info.isImage) {
          const nom = generateImageId(info.ext);
          file.mv(`${DIR_IMG_PATH}/${nom}`);
          auth.user = { ...auth.user, imageProfil: nom, smallImage: false };
          Jimp.read(file.data).then((image) => {
            if (image.bitmap.width > 300) {
              auth.user.smallImage = true;
              image.resize(300, Jimp.AUTO);
              image.write(`${DIR_SM_IMG_PATH}/${nom}`);
            }
            console.log(auth);
            sendSignUpInfos(res, path, auth);
          });
        }
      } else {
        sendSignUpInfos(res, path, auth);
      }
    }
  } else {
    res.status(400).json({ msg: "Rquette vide." });
  }
};

/********************ROUTES***************************** */

const app = express();
app.use(cors());
app.use(upload());

app.post("/api/signup/u", (req, res) => {
  signup(req, res, "user/inscription");
});

app.post("/api/signup/b", (req, res) => {
  signup(req, res, "bricopote/inscription");
});

app.listen(`${PORT}`, () => {
  console.log(`Server started on ${PORT}...`);
});
